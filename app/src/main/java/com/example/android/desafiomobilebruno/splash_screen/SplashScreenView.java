package com.example.android.desafiomobilebruno.splash_screen;

import com.example.android.desafiomobilebruno.entity.EntidadeSocialList;

interface SplashScreenView {

    void exibeEntidadesSociais(EntidadeSocialList list);
    void mostraMensagem(String msg);
    void salvaDadosOffline(String json);
    String carregaDadosOffline();

}
