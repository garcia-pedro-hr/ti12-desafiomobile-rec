package com.example.android.desafiomobilebruno.splash_screen;

import android.os.Handler;
import android.util.Log;

import com.example.android.desafiomobilebruno.entity.EntidadeSocialList;
import com.example.android.desafiomobilebruno.network.api.AppApi;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreenPresenter {

    SplashScreenView view;

    public SplashScreenPresenter(SplashScreenView view) {
        this.view = view;
    }

    public void abreApp() {
        //gera o atraso de 2 segundos para exibição da splash screen
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                baixaDados();
            }
        }, 2000);
    }

    void baixaDados() {
        final AppApi api = AppApi.getInstance();
        api.getSociais().enqueue(new Callback<EntidadeSocialList>() {
            @Override
            public void onResponse(Call<EntidadeSocialList> call, Response<EntidadeSocialList> response) {
                EntidadeSocialList list = response.body();

                if(list != null && list.getEntidadeSocialList() != null) {
                    view.salvaDadosOffline(new Gson().toJson(list));
                    view.exibeEntidadesSociais(list);
                } else {
                    trabalhaOffline();
                }
            }

            @Override
            public void onFailure(Call<EntidadeSocialList> call, Throwable t) {
                trabalhaOffline();
                Log.e("network", t.toString());
            }
        });
    }

    void trabalhaOffline() {
        view.mostraMensagem("Trabalhando offline");

        String jsonEntidadesSociais = view.carregaDadosOffline();

        if(jsonEntidadesSociais != null && !jsonEntidadesSociais.equals("")) {
            view.exibeEntidadesSociais(new Gson().fromJson(jsonEntidadesSociais, EntidadeSocialList.class));
        } else {
            view.exibeEntidadesSociais(null);
        }
    }

}
