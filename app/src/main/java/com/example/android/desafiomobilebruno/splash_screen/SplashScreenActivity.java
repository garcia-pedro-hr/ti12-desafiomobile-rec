package com.example.android.desafiomobilebruno.splash_screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.android.desafiomobilebruno.R;
import com.example.android.desafiomobilebruno.entity.EntidadeSocialList;
import com.example.android.desafiomobilebruno.entidades_sociais.EntidadesSociaisActivity;

public class SplashScreenActivity extends AppCompatActivity implements SplashScreenView {

    private static final String ENTIDADES_SOCIAIS = "entidades_sociais";
    private static final String ENTIDADES_SOCIAIS_JSON = "entidades_sociais_json";

    SplashScreenPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        presenter = new SplashScreenPresenter(SplashScreenActivity.this);

        presenter.abreApp();
    }

    @Override
    public void salvaDadosOffline(String jsonEntidadesSociais) {
        SharedPreferences.Editor editor = getSharedPreferences(ENTIDADES_SOCIAIS, MODE_PRIVATE).edit();
        editor.putString(ENTIDADES_SOCIAIS_JSON, jsonEntidadesSociais);
        editor.apply();
    }

    @Override
    public String carregaDadosOffline() {
        SharedPreferences sharedPreferences = getSharedPreferences(ENTIDADES_SOCIAIS, MODE_PRIVATE);
        String jsonEntidadesSociais = sharedPreferences.getString(ENTIDADES_SOCIAIS_JSON, null);
        return jsonEntidadesSociais;
    }

    //exibe as entidades
    @Override
    public void exibeEntidadesSociais(EntidadeSocialList entidadeSocialList) {
        Intent abreEntidadesSociais = new Intent(SplashScreenActivity.this, EntidadesSociaisActivity.class);
        abreEntidadesSociais.putExtra("entidade_social_list", entidadeSocialList);
        startActivity(abreEntidadesSociais);

    }

    @Override
    public void mostraMensagem(String msg) {
        Toast.makeText(SplashScreenActivity.this, msg, Toast.LENGTH_SHORT).show();
    }
}
